import * as React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import Button from "@mui/material/Button";
import SendIcon from "@mui/icons-material/Send";
import Grid from "@mui/material/Grid";
import axios from "axios";
import TextField from "@mui/material/TextField";
import Alert from "@mui/material/Alert";
import Box from "@mui/material/Box";
import Dialog, { DialogProps } from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import Switch from "@mui/material/Switch";

const back = "http://127.0.0.1:8000/api/";

export const SalesComponent: React.FC<{}> = () => {
  const [products, setProducts] = React.useState([]);
  const [totalPrice, setTotalPrice] = React.useState<number>(0);
  const [listProducts, setListProducts] = React.useState<any>([]);
  const [errorAlert, setErrorAlert] = React.useState<boolean>(false);
  const [errorProduct, setErrorProduct] = React.useState<any>({});
  const [open, setOpen] = React.useState(false);

  const [invoiceData, setInvoiceData] = React.useState<any>({
    client_name: "",
    client_direction: "",
    client_dni: "",
    client_phone: "",
    seller_name: "Brandon Pacheco",
    pay_method: "",
    products: JSON.stringify(listProducts),
    total: totalPrice
  });

  const getInvoiceData = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInvoiceData({
      ...invoiceData,
      [e.target.name]: e.target.value,
    });
    
  };

  const handleSubmit = (e: any) => {
    axios.post(`${back}invoice/`, {
        client_direction: invoiceData.client_direction,
        client_name: invoiceData.client_name,
        client_dni: invoiceData.client_dni,
        client_phone: invoiceData.client_phone,
        seller_name: invoiceData.seller_name,
        products: invoiceData.products,
        pay_method: invoiceData.pay_method,
        total: invoiceData.total,
    }).then((response: any) => {
      setOpen(false)
    })
  };

  const handleChange = (e: SelectChangeEvent) => {
    setInvoiceData({
        ...invoiceData,
        [e.target.name]: e.target.value,
      });
  };

  React.useEffect(() => {
    axios.get(`${back}products/`).then((response: any) => {
      setProducts(response.data);
    });
    if (!products) return undefined;
  }, []);

  React.useEffect(() => {
    let total: number = 0;
    listProducts.forEach((product: any) => {
      total = total + product.totalSelected * product.price;
    });
    setTotalPrice(total);
  }, [listProducts]);

  React.useEffect(() => {
    setInvoiceData({
        ...invoiceData,
        ["products"]: JSON.stringify(listProducts)
      });
  }, [listProducts]);

  React.useEffect(() => {
    setInvoiceData({
        ...invoiceData,
        ["total"]: totalPrice
      });
  }, [totalPrice]);


  function getProductSelected(productSelected: any, event: any) {
    const totalTarget = event.target.value;
    const match = listProducts.findIndex(
      (pr: any) => pr.id === productSelected.id
    );
    if (totalTarget === "" && match !== -1) {
      const copyData = [...listProducts];
      copyData.splice(match, 1);
      setListProducts(copyData);
      return;
    }

    if (match === -1) {
      const data = {
        ...productSelected,
        totalSelected: parseInt(totalTarget),
      };
      listProducts.length === 0
        ? setListProducts([data])
        : setListProducts([...listProducts, data]);
    } else {
      const copyData = [...listProducts];
      copyData[match].totalSelected = parseInt(totalTarget);
      setListProducts(copyData);
    }
    validate();
  }

  const validate = () => {
    listProducts.forEach((product: any) => {
      if (product.totalSelected > product.stock) {
        setErrorAlert(true);
        setErrorProduct(product);
      } else {
        setErrorAlert(false);
        setErrorProduct({});
      }
    });
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <h3>Lista de productos</h3>
        </Grid>
        <Grid item xs={3}>
          <h3>Producto</h3>
        </Grid>
        <Grid item xs={3}>
          <h3>Precio</h3>
        </Grid>
        <Grid item xs={3}>
          <h3>Stock</h3>
        </Grid>
        <Grid item xs={3}>
          <h3>Cantidad</h3>
        </Grid>
      </Grid>
      <List
        sx={{
          width: "100%",
          bgcolor: "background.paper",
          height: "10%",
          overflow: "scroll",
          overflowX: "hidden",
          maxHeight: "700px",
        }}
      >
        <Grid container spacing={2}>
          {products.length > 0
            ? products?.map((product: any) => (
                <Grid container spacing={2}>
                  <Grid item xs={3}>
                    <ListItem>
                      <ListItemText primary={product.name} />
                    </ListItem>
                  </Grid>
                  <Grid item xs={3}>
                    <ListItem>
                      <ListItemText primary={product.price} />
                    </ListItem>
                  </Grid>
                  <Grid item xs={3}>
                    <ListItem>
                      <ListItemText primary={product.stock} />
                    </ListItem>
                  </Grid>
                  <Grid item xs={3}>
                    <ListItem>
                      <TextField
                        id="outlined-number"
                        label="Cantidad"
                        type="number"
                        onKeyUp={(event) => getProductSelected(product, event)}
                      />
                    </ListItem>
                  </Grid>
                </Grid>
              ))
            : null}
        </Grid>
      </List>
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="center"
      >
        <Grid>
          <h3>
            <strong>Total:</strong> {totalPrice}$
          </h3>
        </Grid>
        <Grid>
          <Button
            variant="contained"
            endIcon={<SendIcon />}
            disabled={errorAlert}
            onClick={handleClickOpen}
          >
            Comprar
          </Button>
        </Grid>
        <Grid item xs={12}>
          {errorAlert ? (
            <Alert severity="error">
              Has seleccionado <strong>{errorProduct.totalSelected}</strong> del
              producto <strong>{errorProduct.name}</strong> y solo tenemos
              disponible/es <strong>{errorProduct.stock}</strong>. Porfavor
              seleccione una cantidad menor.
            </Alert>
          ) : (
            <div></div>
          )}
        </Grid>
      </Grid>

      <React.Fragment>
        <Dialog
          fullWidth={true}
          maxWidth={"lg"}
          open={open}
          onClose={handleClose}
        >
          <DialogTitle>
            <Grid
              container
              direction="row"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item xs={12}>
                Facturación
              </Grid>
            </Grid>
          </DialogTitle>
          <DialogContent>
            {listProducts.length === 0 ? (
              <DialogContentText>
                Debe seleccionar productos antes de pagar.
              </DialogContentText>
            ) : (
              <div>
                <Grid container spacing={2}>
                  <Grid item xs={4}>
                    <h3>Producto</h3>
                  </Grid>
                  <Grid item xs={4}>
                    <h3>Precio</h3>
                  </Grid>
                  <Grid item xs={4}>
                    <h3>Cantidad</h3>
                  </Grid>
                </Grid>
                <List
                  sx={{
                    width: "100%",
                    bgcolor: "background.paper",
                    height: "10%",
                    overflow: "scroll",
                    overflowX: "hidden",
                    maxHeight: "500px",
                  }}
                >
                  {listProducts.map((product: any) => (
                    <Grid container spacing={2}>
                      <Grid item xs={4}>
                        <ListItem>
                          <ListItemText primary={product.name} />
                        </ListItem>
                      </Grid>
                      <Grid item xs={4}>
                        <ListItem>
                          <ListItemText primary={product.price + "$"} />
                        </ListItem>
                      </Grid>
                      <Grid item xs={4}>
                        <ListItem>
                          <ListItemText
                            primary={product.totalSelected + " und"}
                          />
                        </ListItem>
                      </Grid>
                    </Grid>
                  ))}
                </List>
                <Box component="form" onSubmit={handleSubmit}>
                  <Grid
                    container
                    direction="row-reverse"
                    justifyContent="center"
                    alignItems="center"
                    spacing={2}
                  >
                    <Grid item xs={4}>
                      <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">
                          Método de pago
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          label="Método de pago"
                          name="pay_method"
                          required
                          onChange={handleChange}>

                          <MenuItem value={"Divisa"}>Divisas</MenuItem>
                          <MenuItem value={"Efectivo"}>Efectivo</MenuItem>
                          <MenuItem value={"Punto de venta"}>
                            Punto de venta
                          </MenuItem>
                          <MenuItem value={"Pago móvil"}>Pago móvil</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item xs={4}>
                      <TextField
                        id="outlined-basic"
                        label="Nombre"
                        name="client_name"
                        variant="outlined"
                        required
                        onChange={getInvoiceData}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <TextField
                        id="outlined-basic"
                        label="Cédula"
                        name="client_dni"
                        variant="outlined"
                        required
                        onChange={getInvoiceData}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <TextField
                        id="outlined-basic"
                        name="client_phone"
                        label="Número de telefono"
                        variant="outlined"
                        required
                        onChange={getInvoiceData}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <TextField
                        id="outlined-basic"
                        label="Dirección"
                        name="client_direction"
                        variant="outlined"
                        required
                        onChange={getInvoiceData}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <TextField
                        id="outlined-basic"
                        label="Vendedor"
                        name="seller_name"
                        variant="outlined"
                        value={"Brandon Pacheco"}
                        required
                        disabled
                      />
                    </Grid>
                  </Grid>
                </Box>
              </div>
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={handleSubmit}>Realizar facturación</Button>
            <Button onClick={handleClose}>Cerrar</Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    </div>
  );
};

export default SalesComponent;
