import React from "react";
import FindInPageIcon from "@mui/icons-material/FindInPage";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import { Paper } from "@mui/material";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import Button from "@mui/material/Button";
import TableRow from "@mui/material/TableRow";
import List from "@mui/material/List";
import Avatar from "@mui/material/Avatar";
import ListItem from "@mui/material/ListItem";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import ListItemText from "@mui/material/ListItemText";
import DialogTitle from "@mui/material/DialogTitle";
import Dialog from "@mui/material/Dialog";
import PersonIcon from "@mui/icons-material/Person";
import Slide from "@mui/material/Slide";
import { TransitionProps } from "@mui/material/transitions";
import { blue } from "@mui/material/colors";
import { Grid } from "@mui/material";
// Styles
import "../styles/history.css";
import axios from "axios";
import moment from "moment";
import "moment/locale/es";
moment.locale("es");

const back = "http://127.0.0.1:8000/api/";
const tasaBCV: number = 11.86;

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

interface Column {
  id: "firstName" | "dni" | "lastName" | "phone" | "email";
  label: string;
  minWidth?: number;
  align?: "right";
  format?: (value: number) => string;
}

const emails = ["username@gmail.com", "user02@gmail.com"];

interface Data {
  client_direction: string;
  client_dni: string;
  client_name: string;
  client_phone: string;
  date: string;
  pay_method: string;
  products?: any[];
  seller_name: string;
  total: number;
}

function createData(
  client_direction: string,
  client_dni: string,
  client_name: string,
  client_phone: string,
  date: string,
  pay_method: string,
  products: any[],
  seller_name: string,
  total: number
): Data {
  return {
    client_direction,
    client_dni,
    client_name,
    client_phone,
    date,
    pay_method,
    products,
    seller_name,
    total,
  };
}

export const HistoryComponent: React.FC<{}> = () => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [open, setOpen] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState(emails[1]);
  const [history, setHistory] = React.useState<any[]>([]);
  const [sectRow, setSecRow] = React.useState<any[]>([]);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  React.useEffect(() => {
    let data: any[] = [];
    axios.get(`${back}invoice/`).then((response: any) => {
      response.data.forEach((responseItem: any) => {
        data.push(
          createData(
            `${responseItem.client_direction}`,
            `${responseItem.client_dni}`,
            `${responseItem.client_name}`,
            `${responseItem.client_phone}`,
            `${moment(responseItem.date).format("dd-MM-YYYY")}`,
            `${responseItem.pay_method}`,
            JSON.parse(responseItem.products),
            `${responseItem.seller_name}`,
            responseItem.total
          )
        );
      });
      setHistory(data);
    });
  }, []);

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleClickOpen = (row: any) => {
    setOpen(true);
    setSecRow(row);
  };

  const handleClose = (value: string) => {
    setOpen(false);
    setSelectedValue(value);
  };

  return (
    <div>
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="baseline"
      >
        <h2>Historial de ventas</h2>
      </Grid>

      <Paper sx={{ width: "100%", overflow: "hidden" }}>
        <TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10]}
            component="div"
            count={history.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
          <Table aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell align="center">
                  <strong>Cliente</strong>
                </TableCell>
                <TableCell align="center">
                  <strong>Cédula cliente</strong>
                </TableCell>
                <TableCell align="center">
                  <strong>Fecha</strong>
                </TableCell>
                <TableCell align="center">
                  <strong>Vendedor</strong>
                </TableCell>
                <TableCell align="center">
                  <strong>Método de pago</strong>
                </TableCell>
                <TableCell align="center">
                  <strong>Total</strong>
                </TableCell>
                <TableCell align="center">
                  <strong>Acciónes</strong>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {history
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row: any) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1}>
                      <TableCell align="center">{row.client_name}</TableCell>
                      <TableCell align="center">{row.client_dni}</TableCell>
                      <TableCell align="center">{row.date}</TableCell>
                      <TableCell align="center">{row.seller_name}</TableCell>
                      <TableCell align="center">{row.pay_method}</TableCell>
                      <TableCell align="center">{row.total}$</TableCell>
                      <TableCell align="center">
                        <Tooltip title="Detalle">
                          <IconButton>
                            <FindInPageIcon
                              onClick={() => handleClickOpen(row)}
                            />
                          </IconButton>
                        </Tooltip>
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>

      <SimpleDialog
        selectedValue={selectedValue}
        selectedRow={sectRow}
        open={open}
        TransitionComponent={Transition}
        onClose={handleClose}
      />
    </div>
  );
};

export interface SimpleDialogProps {
  open: boolean;
  selectedValue: string;
  selectedRow: any;
  TransitionComponent: any;
  onClose: (value: string) => void;
}

function SimpleDialog(props: SimpleDialogProps) {
  const { onClose, selectedValue, selectedRow, open } = props;

  const handleClose = () => {
    onClose(selectedValue);
  };

  const handleListItemClick = (value: string) => {
    onClose(value);
  };

  React.useEffect(() => {}, [selectedRow]);

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>Detalle de venta</DialogTitle>
      <List sx={{ pt: 0 }}>{}</List>
      <List sx={{ pt: 0 }}>
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="center"
        >
          <Grid item xs={6}>
            <ListItem>
              <h3>
                <strong>Cliente: </strong>
              </h3>
            </ListItem>
          </Grid>
          <Grid item xs={3}>
            <ListItem>
              <h3>{selectedRow.client_name}</h3>
            </ListItem>
          </Grid>

          <Grid item xs={6}>
            <ListItem>
              <h3>
                <strong>Dirección cliente: </strong>
              </h3>
            </ListItem>
          </Grid>
          <Grid item xs={3}>
            <ListItem>
              <h3>{selectedRow.client_direction}</h3>
            </ListItem>
          </Grid>

          <Grid item xs={6}>
            <ListItem>
              <h3>
                <strong>Cédula cliente: </strong>
              </h3>
            </ListItem>
          </Grid>
          <Grid item xs={3}>
            <ListItem>
              <h3>{selectedRow.client_dni}</h3>
            </ListItem>
          </Grid>

          <Grid item xs={6}>
            <ListItem>
              <h3>
                <strong>Teléfono cliente: </strong>
              </h3>
            </ListItem>
          </Grid>
          <Grid item xs={3}>
            <ListItem>
              <h3>{selectedRow.client_phone}</h3>
            </ListItem>
          </Grid>

          <Grid item xs={6}>
            <ListItem>
              <h3>
                <strong>Fecha de compra: </strong>
              </h3>
            </ListItem>
          </Grid>
          <Grid item xs={3}>
            <ListItem>
              <h3>{moment(selectedRow.date).format("dd-MM-YYYY")}</h3>
            </ListItem>
          </Grid>

          <Grid item xs={6}>
            <ListItem>
              <h3>
                <strong>Método de pago: </strong>
              </h3>
            </ListItem>
          </Grid>
          <Grid item xs={3}>
            <ListItem>
              <h3>{selectedRow.pay_method}</h3>
            </ListItem>
          </Grid>

          <Grid item xs={6}>
            <ListItem>
              <h3>
                <strong>Nombre del vendedor: </strong>
              </h3>
            </ListItem>
          </Grid>
          <Grid item xs={3}>
            <ListItem>
              <h3>{selectedRow.seller_name}</h3>
            </ListItem>
          </Grid>

          <Grid item xs={12}>
            <ListItem>
              <h3>
                <strong>Productos: </strong>
              </h3>
            </ListItem>
          </Grid>
          <List
            sx={{
              width: "100%",
              bgcolor: "background.paper",
              height: "10%",
              overflow: "scroll",
              overflowX: "hidden",
              maxHeight: "200px",
              marginLeft: "20px",
              marginRight: "20px",
            }}
          >
            <Grid container spacing={2}>
              {selectedRow.products?.map((product: any) => (
                <Grid container spacing={2}>
                  <Grid item xs={4}>
                    <ListItem>
                      <p>{product.name}</p>
                    </ListItem>
                  </Grid>
                  <Grid item xs={4}>
                    <ListItem>
                      <p>{product.price}$</p>
                    </ListItem>
                  </Grid>
                  <Grid item xs={4}>
                    <ListItem>
                      <p>{product.totalSelected} unidades</p>
                    </ListItem>
                  </Grid>
                </Grid>
              ))}
            </Grid>
          </List>

          <Grid item xs={6}>
            <ListItem>
              <h3>
                <strong>Total: </strong>
              </h3>
            </ListItem>
          </Grid>
          <Grid item xs={3}>
            <ListItem>
              <h3>{selectedRow.total}$</h3>
            </ListItem>
          </Grid>
        </Grid>
      </List>
    </Dialog>
  );
}

export default HistoryComponent;
