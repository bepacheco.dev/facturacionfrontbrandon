export interface ATMTableInterface {
}

export interface RegisterInterface {
    username:       string;
    password1:      string;
    password2:      string;
    dni:            string;
    email:          string;
    firstName:      string;
    lastName:       string;
    phone:          string;
}