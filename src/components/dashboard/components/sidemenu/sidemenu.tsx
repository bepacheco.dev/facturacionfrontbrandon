import React from 'react';
import { NavLink, Outlet } from 'react-router-dom';
// Styles
import './sidemenu.css'
// Material UI
import PointOfSaleIcon from '@mui/icons-material/PointOfSale';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import TimelineIcon from '@mui/icons-material/Timeline';
import PeopleOutlineIcon from '@mui/icons-material/PeopleOutline';
import MenuIcon from '@mui/icons-material/Menu';
import LogoutIcon from '@mui/icons-material/Logout';

export const SideMenuComponent: React.FC<any> = () => {

    const [isOpen, setIsOpen] = React.useState<boolean>(false);

    const toggle = () => setIsOpen(!isOpen);

    const menuItem = [
        {
            path: '/dashboard/sales',
            name: 'Facturacion',
            icon: <PointOfSaleIcon />
        },
        {
            path: '/dashboard/history',
            name: 'Historial de ventas',
            icon: <TimelineIcon />
        }
    ]

    return(
        <div className='container'>
            <div style={{width: isOpen ? '350px' : '50px'}} className="sidebar">
                <div className="top_section">
                    <h1 style={{display: isOpen ? 'block' : 'none' }} className="logo">Facturación</h1>
                    <div style={{marginLeft: isOpen ? '50%' : '0%' }} className="bars">
                        <MenuIcon onClick={toggle} />
                    </div>
                </div>
                {
                    menuItem.map((item, index) => (
                        <NavLink to={item.path} key={index} className="link">
                            <div className="icon">{item.icon}</div>
                            <div style={{display: isOpen ? 'block' : 'none'}} className="link_text">{item.name}</div>
                        </NavLink>
                        
                    ))
                }
            </div>
            <main style={{height: '100%' }}>
                <Outlet />
            </main>
        </div>
    )
}

export default SideMenuComponent;