import React, {lazy, Suspense} from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
// Components
import { SideMenuComponent } from "./components/dashboard/components/sidemenu/sidemenu";
// Lazy
const LazyHistory = lazy(() => import("./components/dashboard/pages/history"));
const LazySales = lazy(() => import("./components/dashboard/pages/sales"));

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
      <Route
          path="/"
          element={
            <Suspense fallback={<div>Cargando...</div>}>
              <Navigate to="/dashboard" />
            </Suspense>
          }
        />
        <Route path="/dashboard" element={<SideMenuComponent />}>
        <Route path="/dashboard/" element={
            <Suspense fallback={<div>Cargando...</div>}>
              <Navigate to="sales" />
            </Suspense>
          } />
          <Route path="history" element={
            <Suspense fallback={<div>Cargando...</div>}>
            <LazyHistory />
          </Suspense>
        } />
          <Route path="sales" element={
            <Suspense fallback={<div>Cargando...</div>}>
            <LazySales />
          </Suspense>
        } />
        <Route path="/dashboard/close" element={
            <Suspense fallback={<div>Cargando...</div>}>
              <Navigate to="/" />
            </Suspense>
          } />
        </Route>
                    
          
        
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

reportWebVitals();
